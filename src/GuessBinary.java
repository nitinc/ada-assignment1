/**
 * 
 */

/**
 * @author nitin
 *
 */
public class GuessBinary {
	static int secret = 251;//Our secret code
	
	public static void main(String[] args) {
		
		GUESSRANGE(1,500);//if n=100
	}

	static int GUESSRANGE(int imin, int imax)
	{
	      // calculate midpoint to cut the set in half
	      int guess = (imin+imax)/2;
	 
	      // three-way comparison
	      if (CHECK(guess) == 1){
	        // guess is in lower subset (left side)
	    	  System.out.println("Inside Left Array:" + guess);//test the guesses
	        return GUESSRANGE(imin, guess-1);
	      }
	
	      else if (CHECK(guess) == -1){
	        // guess is in upper subset (Right Side)
	    	  System.out.println("Inside Right Array:" + guess);//test the guesses
	        return GUESSRANGE(guess+1, imax);
	      }
	      else{
	    	  // guess has been found
	    	  System.out.println("SUCCESS:          " + guess);
		        return guess;
	    	  
	      }
	       
	}

	static int CHECK(int p){
		if (p == secret) return 0;
		else if (p < secret) return -1;
		//else if (p > secret) return 1;
		else return 1;
	}
}


